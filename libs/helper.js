const GLOBALS = {
    timerId: 1,
    revocationId: []
};
let parser = require('cheerio');
let log = require('./logger').log;
let debug = require('./logger').debug;


function getRevocationIds(data) {
    debug('Получаем ID для возврата...');
    let html = parser.load(data.results.value.page);
    let ids = html('tr[_reserve_id]');
    
    debug(`Получили ID для возврата`, ids);
    if (ids.length > 1) {
        return [ids[0].attribs['_reserve_id'], ids[1].attribs['_reserve_id']];
    } else {
        return [ids[0].attribs['_reserve_id']];
    }
}


module.exports = {
    GLOBALS,
    attemptsLimit: (data, limitAttempts, callback) => {
        debug(`Количество попыток ${limitAttempts} из 10`);
        log('Попытка забронировать №', limitAttempts);

        if (limitAttempts >= 10) {
            log('Количество попыток превышает 10');
            log('--------------------------------------------');
            log(data.value);

            process.exit(0);
        } else {
            callback();
        }
    },
    getRevocationIds: getRevocationIds,
    rehold: (revocation, revocation_ids, options) => {
        debug(`Начинаю снимать бронь...`);
        revocation(revocation_ids, options.options.headers['Cookie']).then((data) => {
            debug(`Бронь снята`);
            debug(`Вызываю перебронировку`);
            log('Билеты возвращены!');

            options.hold();
        });
    },
};