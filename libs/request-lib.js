let request = require('request');
let debug = require('../libs/logger').debug;

module.exports = function (url) {
    let _url = url;
    debug('RequestLib создан');

    return {
        sendData: (requestConfig, callback) => {
            if (!_url) {
                console.log('Забыл передать параметр url RequestLib.js');
            }
            debug('Делаем запрос на сервер...');
            request({
                har: {
                    url: _url,
                    method: 'POST',
                    headers: requestConfig.headers,
                    postData: {
                        mimeType: 'application/x-www-form-urlencoded',
                        params: requestConfig.postData.params
                    }
                }
            }, (trash, response) => {
                debug('Получили ответ с сервера');
                if (response) {
                    if (response.statusCode === 200 && response.statusMessage === "OK") {
                        debug('Запрос прошёл без ошибок');
                        let body = JSON.parse(response.body);
                        let error = body.error;

                        if (!error) {
                            callback({error: false, results: body});
                        } else {
                            callback({error: true, results: 'Ошибка на сервере...', value: body.value});
                        }
                    } else {
                        debug('Запрос прошёл c ошибками');
                        callback({
                            error: true,
                            results: `Ошибка запроса: ${response.statusCode}:${response.statusMessage}`
                        });
                    }
                } else {
                    debug('Запрос выдал неизвестную ошибку');
                    callback({error: true, results: `Неивестная ошибка ( куки, токен и.т.д)`});
                }
            });
        }
    }
};