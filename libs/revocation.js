let requestConfig = require('./request-config');
let holder = require('./holder');
let requestLib = require('./request-lib')('http://booking.uz.gov.ua/ru/cart/revocation/');
let debug = require('../libs/logger').debug;


module.exports = function (cookie){
    requestConfig.setCookie(cookie);
    debug('Инициализируем библиотеку снятия брони...');
    return (revocation_ids, cookie = undefined) => {
        debug('Инициализация прошла успешно!');

        return new Promise((resolve) => {
            let ids = holder(['reserve_ids', revocation_ids]);

            if(cookie) requestConfig.setCookie(cookie);

            debug('Снимаем бронь...');
            requestLib.sendData(requestConfig.init(ids), (data) => {
                resolve(data);
            });
        });
    }
};