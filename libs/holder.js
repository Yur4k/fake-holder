let names = require('../data/names');
let debug = require('../libs/logger').debug;
let wagon_types = ['С1', 'С2', 'С3', 'П', 'К', 'Л'];

function setChartline(wagon_type) {
    debug(`Определяем [chartline]`);
    if (wagon_type === 'П' || wagon_type === 'К') return 'Б';
    else return 'А';
}

function fixPlaceLength(place) {
    let placeString = ''+place;

    debug(`Проверяем формат записи мест перед отправкой`);

    if (placeString.length === 3) return `${place}`;
    else if (placeString.length === 2) return `0${place}`;
    else return `00${place}`;
}

module.exports = (data) => {
    if(Array.isArray(data)) {
        debug('Установка параметров для formData БЕЗ формы бронирования');
        let params = [];

        data[1].forEach((value, index) => {
            params.push({
                name: `${data[0]}[${index}]`,
                value: value
            })
        });

        return params;
    }

    debug('Установка параметров для formData ВКЛЮЧАЯ форму бронирования');
    let holdPlaces = [];
    let trainParams = [
        {
            name: 'round_trip',
            value: 0
        },
        {
            name: 'train',
            value: data.train
        },
        {
            name: 'from',
            value: data.from
        },
        {
            name: 'to',
            value: data.to
        },
        {
            name: 'date',
            value: data.date
        }
    ];


    data.places.forEach((place, index) => {
        holdPlaces.push({
            name: `places[${index}][wagon_type]`,
            value: data.wagon_type
        });
        holdPlaces.push({
            name: `places[${index}][wagon_num]`,
            value: data.wagon_num
        });
        holdPlaces.push({
            name: `places[${index}][wagon_class]`,
            value: data.wagon_class
        });
        holdPlaces.push({
            name: `places[${index}][place_num]`,
            value: fixPlaceLength(place)
        });
        holdPlaces.push({
            name: `places[${index}][ord]`,
            value: 0
        });
        holdPlaces.push({
            name: `places[${index}][bedding]`,
            value: ''
        });
        holdPlaces.push({
            name: `places[${index}][child]`,
            value: ''
        });
        holdPlaces.push({
            name: `places[${index}][stud]`,
            value: ''
        });
        holdPlaces.push({
            name: `places[${index}][transportation]`,
            value: 0
        });
        holdPlaces.push({
            name: `places[${index}][reserve]`,
            value: 0
        });
        holdPlaces.push({
            name: `places[${index}][charline]`,
            value: setChartline(data.wagon_type)
        });
        holdPlaces.push({
            name: `places[${index}][firstname]`,
            value: names[index].firstname
        });
        holdPlaces.push({
            name: `places[${index}][lastname]`,
            value: names[index].lastname
        });
    });

    holdPlaces.push({
        name: `captcha`,
        value: ''
    });

    return trainParams.concat(holdPlaces);

};