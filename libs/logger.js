let config = {
    'log': {
        enabled: true
    },
    'debug': {
        debugNotifications: false,
        debugNotificationsSeparate: false,
    }
};

module.exports = {
    get log() {
        return function (params) {
            let argsArray = [].slice.call(arguments);

            if (config['log'].enabled) {
                console.log.apply(console, argsArray);
            }
        }
    },

    get debug() {
        return function (params) {

            let argsArray = [].slice.call(arguments);
            if (config['debug'].debugNotifications) {
                if (config['debug'].debugNotificationsSeparate) {
                    log('------------------------------');
                    console.log.apply(console, argsArray);
                    log('------------------------------');
                } else {
                    console.log.apply(console, argsArray);
                }

            }
        }
    }
};
