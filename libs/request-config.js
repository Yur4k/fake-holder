let debug = require('../libs/logger').debug;

module.exports = {
    init: function(params) {
        debug('Устанавливаю параметры запроса');
        this.postData.params = params;

        return this;
    },
    setCookie: function (cookie) {
        debug('Устанавливаю куки в заголовок запроса');
        this.headers.forEach((header) => {
            if (header.name === "Cookie") {
                header.value = cookie;

                return true;
            }
        });
    },
    headers: [
        {
            name: 'Cookie',
            value: ''
        },
        {
            name: 'Accept',
            value: '*/*'
        },
        {
            name: 'Accept-Encoding',
            value: 'gzip, deflate'
        },
        {
            name: 'Accept-Language',
            value: 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
        },
        {
            name: 'Connection',
            value: 'keep-alive'
        },
        {
            name: 'Content-Type',
            value: 'application/x-www-form-urlencoded'
        },
        {
            name: 'GV-Ajax',
            value: '1'
        },
        {
            name: 'GV-Referer',
            value: 'http://booking.uz.gov.ua/ru/'
        },
        {
            name: 'GV-Screen',
            value: '1440x900'
        },
        {
            name: 'Accept-Language',
            value: 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
        },
        {
            name: 'Host',
            value: 'booking.uz.gov.ua'
        },
        {
            name: 'X-Compress',
            value: 'null',
        },
        {
            name: 'Referer',
            value: 'http://booking.uz.gov.ua/ru/'
        },
        {
            name: 'User-Agent',
            value: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
        },
        {
            name: 'Origin',
            value: 'http://booking.uz.gov.ua'
        }
    ],
    postData: {
        mimeType: 'application/x-www-form-urlencoded',
        params: []
    }
};