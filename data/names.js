module.exports = [
    {
        firstname: 'Дмитрий',
        lastname: 'Песков'
    },
    {
        firstname: 'Анастасия',
        lastname: 'Зверюгова'
    },
    {
        firstname: 'Денис',
        lastname: 'Попков'
    },
    {
        firstname: 'Зинаида',
        lastname: 'Лукъяненко'
    },
    {
        firstname: 'Степан',
        lastname: 'Аскелов'
    },
    {
        firstname: 'Николай',
        lastname: 'Щербань'
    },
    {
        firstname: 'Виктория',
        lastname: 'Стеценко'
    },
    {
        firstname: 'Даня',
        lastname: 'Фукин'
    },
    {
        firstname: 'Ибрагим',
        lastname: 'Мушенов'
    },
    {
        firstname: 'Анатолий',
        lastname: 'Корц'
    },
];