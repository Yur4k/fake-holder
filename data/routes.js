const routes = {
    2200040 : 'Конотоп',
    2200001 : 'Киев',
    2204001 : 'Харьков',
    2204465 : 'Змиев',
    2204426 : 'Чугуев'
};

module.exports = {
    byId(id) {
        return routes[id];
    },
    byName(_route) {
        for (let routeId in routes) {
            if(routes[routeId] === _route) return routeId;
        }

        console.log('МАРШРУТ НЕ НАЙДЕН!!!');
        process.exit(0);
    }
};