let requestConfig = require('../libs/request-config');
let requestLib = require('../libs/request-lib')('http://booking.uz.gov.ua/ru/cart/add/');
let holds = require('./hold_parts/holds');
let helper = require('../libs/helper');
let other = require('./hold_parts/other');
let debug = require('../libs/logger').debug;
let captcha = require('./hold_parts/captcha');
let limitAttempts = 0;
let formData;
let options;


module.exports = function hold(_formData, _options) {
    formData = _formData || formData;
    options = _options || options;

    debug('Начиная бронировать...');
    requestConfig.setCookie(options.headers['Cookie']);
    requestLib.sendData(requestConfig.init(formData), (data) => {
        if (!data.error && !data.results.captcha && !data.results.data) {
            holds(data, options, {hold, formData}, () => {
                limitAttempts = 0;
            });
        } else if (!data.results.captcha) {
            other(data, hold);
        } else {
            helper.attemptsLimit(data, limitAttempts++, () => {
                captcha(options, {formData, requestConfig, hold});
            });
        }
    });
};