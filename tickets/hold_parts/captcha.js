let request = require('request');
let fs = require('fs');
let debug = require('../../libs/logger').debug;
let log = require('../../libs/logger').log;
let anticaptcha = require('../../libs/anticaptcha')('9f17742ef573b3212dfdd9b8d8f50ad4');
let setCaptcha = (formData, captcha) => {
    formData.forEach((formItem) => {
        if (formItem.name === 'captcha') {
            formItem.value = captcha;
        }
    });
};

anticaptcha.setMinLength(4);
anticaptcha.setMaxLength(4);

function captcha(options, others) {
    debug('Обнаруже капча! начинаю скачивание и дешифрование.');
    setTimeout( ()=> {
        request(options).pipe(fs.createWriteStream('captcha.gif')).on('close', () => {
            setTimeout( ()=> {
                debug('Решаем капчу...');
                log('решаем капчу...');
                anticaptcha.createImageToTextTask({
                        case: false,
                        body: new Buffer(fs.readFileSync('captcha.gif')).toString('base64')
                    },
                    function (err, taskId) {
                        if (err) {
                            debug('Не получилось загрузить капчу на удаленный сервер дешифрования');
                            log(err);
                            return;
                        }

                        anticaptcha.getTaskSolution(taskId, function (err, taskSolution) {
                            if (err) {
                                log(err);
                                debug('Не получилось загрузить капчу на удаленный сервер дешифрования');

                                return;
                            }
                            debug('Капча разгадана! Устанавливаем капчу в formData');
                            setCaptcha(others.formData, +taskSolution);

                            log('Каптча: ', +taskSolution);
                            others.requestConfig.setCookie(options.headers['Cookie']);
                            others.hold();
                        });
                    }
                );
            }, 1);
        });
    }, 1);
}


module.exports = captcha;