let debug = require('../../libs/logger').debug;
let log = require('../../libs/logger').log;

function other(data, hold) {
    debug('Другая ошибка при бронировке');
    if (data.error && data.value) {
        debug('Билет(ы) уже заняты!');
        debug('Пытаюсь забронировать снова...!');
        log('Билет пока(или уже) занят(ы)!');
        log(data.value);

        hold();
    }
    else if(data.error && !data.value){
        debug('Незвестная ошибка other.js DATA.ERROR && !DATA.VALUE');
        log('Ошибка: ', data.error);
    }else {
        debug('Незвестная ошибка other.js ELSE');
        log('ОШИБКА, ОСТАНОВКА!');

        process.exit(0);
    }
}


module.exports = other;