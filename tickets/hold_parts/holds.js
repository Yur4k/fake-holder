let helper = require('../../libs/helper');
let revocation = require('../../libs/revocation')(null);
let debug = require('../../libs/logger').debug;
let log = require('../../libs/logger').log;

function holds(data, options, others, callback) {
    let revocation_ids = helper.getRevocationIds(data);
    let startTime = `${new Date(new Date().getTime() + 3 * 60* 60 * 1000).toString().substring(4, 24)}`;
    let endTime = `${new Date(new Date().getTime() + ((3 * 60) + 29)* 60 * 1000).toString().substring(4, 24)}`;

    helper.GLOBALS.revocationId = revocation_ids;

    debug('Билет забронирован!');
    debug('IDs', revocation_ids);
    debug('Cookie', options.headers['Cookie']);
    debug('start', startTime);
    debug('end', endTime);

    log('ID для отмены заказа: ', revocation_ids);
    log('Cookie при покупке: ', options.headers['Cookie']);
    log(`Зарезервировано: ${startTime}`);
    log(`Истекает: ${endTime}`);

    debug('Устанавливаю обратный отсчет');
    helper.GLOBALS.timerId = setTimeout(helper.rehold, 1740000,
        revocation,
        revocation_ids,
        {
            hold: others.hold,
            placesCount: others.formData[0].placesCount,
            options
        }
    );

    callback();
}


module.exports = holds;